const khuVucA = "A";
const khuVucB = "B";
const khuVucC = "A";
const khuVucKhac = "X";
const doiTuong1 = "1";
const doiTuong2 = "2";
const doiTuong3 = "3";
const doiTuongKhac = "0";
function tinhDiemCongTheoKhuVuc(khuVuc) {
  if (khuVuc == "A") {
    return 2;
  } else if (khuVuc == "B") {
    return 1;
  } else if (khuVuc == "C") {
    return 0.5;
  } else {
    return 0;
  }
}
function tinhDiemCongTheoDoiTuong(doiTuong) {
  if (doiTuong == "1") {
    return 2.5;
  } else if (doiTuong == "2") {
    return 1.5;
  } else if (doiTuong == "3") {
    return 1;
  } else {
    return 0;
  }
}

function tinhDiemTongKet() {
  var diemChuan = document.getElementById("diemChuan").value * 1;
  var diemThuNhat = document.getElementById("diemThuNhat").value * 1;
  var diemThuHai = document.getElementById("diemThuHai").value * 1;
  var diemThuBa = document.getElementById("diemThuBa").value * 1;
  var khuVuc = document.querySelector('option[name="selector"]:checked').value;
  var diemKhuVuc = tinhDiemCongTheoKhuVuc(khuVuc);
  var doiTuong = document.querySelector(
    'option[name="selector__two"]:checked'
  ).value;
  var diemDoiTuong = tinhDiemCongTheoDoiTuong(doiTuong);
  console.log(khuVuc);
  console.log(doiTuong);
  var diemTongKet =
    diemThuNhat + diemThuHai + diemThuBa + diemKhuVuc + diemDoiTuong;
  console.log(diemTongKet);
  var ketqua = "";
  if (diemTongKet >= diemChuan) {
    ketqua = "Pass";
  } else {
    ketqua = "Failed";
  }

  var contentHtml = `<h3>Điểm tổng kết: ${diemTongKet}đ</h3><h3>Kết quả: ${ketqua}</h3>`;

  document.getElementById("result").innerHTML = contentHtml;
}

function tinhTien(){
    var dienTieuThu=document.getElementById("dienTieuThu").value*1;
    var hoTen=document.getElementById("hoTen").value; 
    var tongTien=0;
    if(dienTieuThu<=50){
        tongTien=dienTieuThu*500;
    }
    else if( dienTieuThu>50 && dienTieuThu <=100 ){
        tongTien=(50*500)+(dienTieuThu-50)*650;
    }
    else if( dienTieuThu >100 && dienTieuThu<=200){
        tongTien=(50*500)+(50*650)+(dienTieuThu-100)*850;
    }
    else if(dienTieuThu >200 && dienTieuThu<=350){
        tongTien=(50*500)+(50*650)+(100*850)+(dienTieuThu-200)*1100;

    }
    else{
        tongTien=tongTien=(50*500)+(50*650)+(100*850)+(150*1100)+(dienTieuThu-350)*1300;
    }
    var contentHtml = `<h3>Họ tên: ${hoTen}</h3><h3>Tiền điện: ${tongTien} VNĐ</h3>`;

  document.getElementById("result2").innerHTML = contentHtml;
}


function tinhTienThue(){
    var hoTenCaNhan=document.getElementById("hoTenCaNhan").value;
    var tongThuNhap=document.getElementById("tongThuNhap").value*1;
    var soNguoiPhuThuoc=document.getElementById("soNguoiPhuThuoc").value*1;
 var thuNhapChiuThue=tongThuNhap-4000000-soNguoiPhuThuoc*1600000;
 var tienNop=0;
var khongHopLe='';
 if(thuNhapChiuThue>=6000000 && thuNhapChiuThue<=60000000){
    tienNop=thuNhapChiuThue*0.05;
 }
 else if(thuNhapChiuThue>60000000 && thuNhapChiuThue<= 120000000){
    tienNop=thuNhapChiuThue*0.1;
 }
 else if(thuNhapChiuThue>120000000 && thuNhapChiuThue<= 210000000){
    tienNop=thuNhapChiuThue*0.15;
 }
 else if(thuNhapChiuThue>210000000 && thuNhapChiuThue<= 384000000){
    tienNop=thuNhapChiuThue*0.2;
 }
 else if(thuNhapChiuThue>384000000 && thuNhapChiuThue<= 624000000){
    tienNop=thuNhapChiuThue*0.25;
 }
 else if(thuNhapChiuThue>624000000 && thuNhapChiuThue<= 920000000){
    tienNop=thuNhapChiuThue*0.3;
 }
 else if(thuNhapChiuThue> 920000000)
 {
    tienNop=thuNhapChiuThue*0.35;
 }
 else {
khongHopLe="Không hợp lệ"

 }
 if(khongHopLe=="Không hợp lệ"){
    var contentHtml = `<h3>Họ tên: ${hoTenCaNhan}</h3><h3>Tiền thu nhập cá nhân: ${khongHopLe} </h3>`;

    document.getElementById("result3").innerHTML = contentHtml;
 }
 else{
    var contentHtml = `<h3>Họ tên: ${hoTenCaNhan}</h3><h3>Tiền thu nhập cá nhân: ${tienNop} </h3>`;

    document.getElementById("result3").innerHTML = contentHtml;
 }
 console.log(tienNop);

}
